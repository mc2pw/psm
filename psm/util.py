

def co_repeat(func):
    arg = yield

    while True:
        arg = yield func(arg)

def co_plug(gen1, gen2):
    has_msg = False
    for msg in gen1:
        has_msg = True
        break

    if not has_msg:
        return

    for x in gen2:
        break

    while True:
        res = gen2.send(msg)
        try:
            msg = gen1.send(res)
        except StopIteration as e:
            return e.value


