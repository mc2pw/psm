import requests
import logging

log = logging.getLogger(__name__)

def find_200(base_url, paths):
    """Finds one 200 response trying paths from find_200/paths.csv."""

    done = False
    for path in paths:
        for method in ('GET', 'POST'):
            log.info('{} {}{}'.format(method, base_url, path))
            response = requests.request(method, base_url + path)
            log.info('{}'.format(response.status_code))
            if response.status_code == 200:
                done = True
                break

        if done:
            return method, path, response

