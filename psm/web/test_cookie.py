import requests
import urllib
import logging

log = logging.getLogger(__name__)

def test_cookie(url, name, value, ses):
    del ses.cookies[name]
    ses.cookies[name] = urllib.parse.quote(value, safe='')
    res = ses.get(url)
    log.debug('{}'.format(res.status_code))

    return res.status_code == 200
