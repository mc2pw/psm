import logging

log = logging.getLogger(__name__)

def prepare_extended_pad(ctext, length, block_pos):
    """Prepares the second-to-last block in order to extend the fake
    padding in the last block, so that an oracle can verify the result
    and confirm the padding.

    The padding is assumed to be PKCS7
    (See: https://tools.ietf.org/html/rfc2315).

    * ctext is a bytearray of the cipher text with fake padding.
    Mutated in place.
    * length is the length of the current fake padding (possibly 0).
    * block_pos is the position of the block to be modified for creating
    the fake padding, counter from the last byte.
    * bs is the block size.
    """

    stop = len(ctext) - block_pos
    for i in range(stop - length, stop):
        ctext[i] ^= length ^ (length + 1)

    return ctext

def possible_extended_pads(ctext, length, block_pos, possible_bytes):
    """Generates all possible extended pads

    ctext is mutated in place.
    """

    prepare_extended_pad(ctext, length, block_pos)
    for byte in possible_bytes:
        log.debug('pos={}, byte={}'.format(length, hex(byte)))
        ctext[len(ctext) - block_pos - length - 1] = byte

        yield ctext

def decrypt_block(ctext, ptext, block_pos, bs, last_unchanged=False):
    """Generates possible pads for a given block and extends them when
    they are correct.

    ctext is mutated in place.
    """

    stop = len(ctext) - block_pos
    original_block = ctext[stop - bs: stop]

    log.info('block_pos={}'.format(block_pos//bs))
    for length in range(bs):
        #if length == 0:
        #    except_byte = ctext[stop - length - 1]
        #else:
        #    raise RuntimeError()

        if block_pos == bs and length == 0:
            last_byte = ctext[stop - 1]
            if last_unchanged:
                possible_bytes = (last_byte,)
            else:
                possible_bytes = (x for x in range(256) if x != last_byte)
        else:
            possible_bytes = range(256)

        for ctext in possible_extended_pads(ctext, length, block_pos, possible_bytes):
            pad_is_good = yield ctext
            if pad_is_good:
                pt_block = bytearray(
                    x ^ (length + 1) ^ original_block[i + bs - length - 1]
                    for i, x in enumerate(ctext[stop - length - 1 : stop])
                )
                log.info('block={}'.format(pt_block))

                if block_pos == bs and not verify_pad(pt_block, bs):
                    raise RuntimeError("Bad pad decrypted")
                break

        if not pad_is_good:
            raise ValueError("No good pad found")

    for i in range(stop - bs, stop):
        original_byte = original_block[i - stop + bs]

        # Store decrypted block.
        ptext[i] = ctext[i] ^ bs ^ original_byte

        # Eliminate fake pad when done.
        ctext[i] = original_byte

    return ctext, ptext


def decrypt(ctext, bs=8, last_unchanged=False):
    def truncated(block_pos, pads):
        pad_is_good = None
        while True:
            try:
                c = pads.send(pad_is_good)
                pad_is_good = yield bytes(c[:len(c)-block_pos+bs])
            except StopIteration as e:
                return e.value

    assert(len(ctext) % bs == 0)
    ctext = bytearray(ctext)
    ptext = bytearray(len(ctext) - bs)

    for block_pos in range(bs, len(ctext), bs):
        ctext, ptext = yield from truncated(
            block_pos,
            decrypt_block(ctext, ptext, block_pos, bs, last_unchanged)
        )

    return bytes(ptext)

def verify_pad(ct, bs=8):
    length = ct[-1]
    return 0 < length <= bs and all(ct[i] == length for i in range(max(-length, -len(ct)), 0))
